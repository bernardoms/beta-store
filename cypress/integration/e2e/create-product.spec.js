/// <reference types="cypress" />

context('Create Product', () => {

    describe("Create Product Fail", ()=>{
        it("should be redirect to list when try to create a product without login", () => {
            cy.visit('/add')
            cy.intercept('GET','/v1/products').as('request');
            cy.location('pathname').should('eq', '/list')
        })

        it("should not create product if there is missing requireded filds", () => {
            cy.visit('/')
    
            cy.login()
    
            cy.location('pathname').should('eq', '/add')

            cy.get('button').click()

            cy.get('div').contains('name field required')
            cy.get('div').contains('image field required')
            cy.get('div').contains('quantity field required')
            cy.get('div').contains('sku field required')
            cy.get('div').contains('price field required')

            cy.location('pathname').should('eq', '/add')
        })
    })
    
    describe("Create Product Success", ()=> {

        after(() => {
            //CLEANING TEST DATA
            cy.request("DELETE", "https://beta-store-product-api.herokuapp.com/v1/products/1234")
        })

        it("should create a product", () => {
            cy.visit('/')
            
            cy.intercept('POST', '**/v1/products').as('postProduct');

            cy.login()

            cy.location('pathname').should('eq', '/add')
    
            cy.get('input[name="image"]').type('http://img-att.com')
            cy.get('input[name="name"]').type('product-test')
            cy.get('input[name="description"]').type('product-description')
            cy.get('input[name="sku"]').type('1234')
            cy.get('input[name="quantity"]').type('1')
            cy.get('input[name="price"]').type('19.99')
    
            cy.get('button').click()

            cy.wait('@postProduct', {timeout:50000});

            cy.get('div').contains('Product created with success!')
            
            cy.visit('/list')
            cy.get('h5').last().invoke('text').should('eq', 'product-test')
            cy.get('div').find('img').last().should('have.attr', 'src').should('include','http://img-att.com')
        })
    })
})
